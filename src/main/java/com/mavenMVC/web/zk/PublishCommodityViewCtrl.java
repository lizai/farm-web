package com.mavenMVC.web.zk;

import com.mavenMVC.dao.ICommodityCategoryDao;
import com.mavenMVC.dao.ICommodityDao;
import com.mavenMVC.entity.Commodity;
import com.mavenMVC.entity.CommodityType;
import com.mavenMVC.entity.Commoditycategory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.util.*;

/**
 * Created by yizhou on 2018/1/22.
 */
public class PublishCommodityViewCtrl extends SelectorComposer<Component> {
    protected final Logger logger = Logger.getLogger(String.valueOf(PublishCommodityViewCtrl.class));

    @Wire
    private Window win;
    @Wire
    private Textbox commodityNameTextbox;
    @Wire
    private Intbox commodityPriceIntbox;
    @Wire
    private Textbox commodityDetailTextbox;
    @Wire
    private Image commodityImage;
    @Wire
    private Selectbox commodityTypeSelectbox;
    @Wire
    private Selectbox commodityCategorySelectbox;
    @Wire
    private Intbox commoditySaleNumTextbox;
    @Wire
    private Button submitBtn;

    @Autowired
    private ICommodityDao iCommodityDao;
    @Autowired
    private ICommodityCategoryDao iCommodityCategoryDao;

    private Window parentWindow;

    private ListModel<Commoditycategory> commodityCategorys;

    private ListModel<CommodityType> commodityTypes;

    private Commodity commodityEntity;

    public PublishCommodityViewCtrl() {
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        Map map = Executions.getCurrent().getArg();
        commodityCategorys = new ListModelList<Commoditycategory>(iCommodityCategoryDao.getList(null, true, 0, Integer.MAX_VALUE, null));

        List<CommodityType> commodityTypeList = new ArrayList<CommodityType>();
        commodityTypeList.add(new CommodityType(0,"生活"));
        commodityTypeList.add(new CommodityType(1,"装修"));
        commodityTypeList.add(new CommodityType(2,"养殖"));
        commodityTypeList.add(new CommodityType(3,"地块"));
        commodityTypes = new ListModelList<CommodityType>(commodityTypeList);

        if(map.get("commodityId")!=null){
            commodityEntity = iCommodityDao.getById(Long.parseLong(map.get("commodityId").toString()));
            for(Commoditycategory i : ((ListModelList<Commoditycategory>) commodityCategorys)){
                if(i.getCommodityCategoryId().equals(commodityEntity.getCommodityCategory())){
                    ((ListModelList<Commoditycategory>)commodityCategorys).addToSelection(i);
                }
            }
        }else {
            commodityEntity = new Commodity();
            commodityEntity.setCommodityCategory(commodityCategorys.getElementAt(0).getCommodityCategoryId());
            ((ListModelList<Commoditycategory>) commodityCategorys).addToSelection(((ListModelList<Commoditycategory>) commodityCategorys).get(0));
            commodityEntity.setCommodityStatus(0);
        }

    }


    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        parentWindow = (Window) win.getParent();
    }

    @Listen("onChange = #commodityNameTextbox")
    public void commodityNameTextbox() {
        String name = commodityNameTextbox.getValue();
        commodityEntity.setCommodityName(name);
    }

    @Listen("onChange = #commodityPriceIntbox")
    public void commodityPriceIntbox() {
        Integer commodityPrice = commodityPriceIntbox.getValue();
        commodityEntity.setCommodityPrice(commodityPrice);
    }

    @Listen("onChange = #commodityDetailTextbox")
    public void commodityDetailTextbox() {
        String commodityDetail = commodityDetailTextbox.getValue();
        commodityEntity.setCommodityDetail(commodityDetail);
    }

    @Listen("onSelect = #commodityCategorySelectbox")
    public void commodityCategorySelectbox() {
        Set<Commoditycategory> types = ((ListModelList<Commoditycategory>) commodityCategorys).getSelection();
        Commoditycategory type = types.iterator().next();
        commodityEntity.setCommodityCategory(type.getCommodityCategoryId());
    }

    @Listen("onChange = #commoditySaleNumTextbox")
    public void commoditySaleNumTextbox() {
        Integer commoditySaleNum = commoditySaleNumTextbox.getValue();
        commodityEntity.setCommoditySaleNum(commoditySaleNum);
    }

    @Listen("onClick = #submitBtn")
    public void submit() {
        logger.info("do submit function");
        iCommodityDao.saveOrUpdateEntity(commodityEntity);
        Messagebox.show("商品信息修改成功");
        win.detach();
        refreshParentWindow();
    }
    public void refreshParentWindow() {
        Button b = (Button) parentWindow.getFellow("search");
        Events.sendEvent(new Event("onClick", b));
    }

    public Commodity getCommodityEntity() {
        return commodityEntity;
    }

    public ListModel<Commoditycategory> getCommodityCategorys() {
        return commodityCategorys;
    }

    public ListModel<CommodityType> getCommodityTypes() {
        return commodityTypes;
    }
}
