package com.mavenMVC.web.zk;

import com.mavenMVC.dao.ICommodityDao;
import com.mavenMVC.entity.Commodity;
import com.mavenMVC.web.zk.renders.BooksDataRender;
import com.mavenMVC.web.zk.renders.CommodityDataRender;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yizhou on 2018/1/21.
 */
public class ShowCommoditiesCtrl extends SelectorComposer<Component> {
    protected final Logger logger = Logger.getLogger(String.valueOf(ShowBooksCtrl.class));
    @Wire
    private Window showCommoditiesWin;
    @Wire
    private Button insertCommodities;
    @Wire
    private Button search;
    @Wire
    private Grid grid;

    @Autowired
    private ICommodityDao iCommodityDao;

    private ListModel<Commodity> commodities;
    private Map authorityMap = new HashMap();

    public ShowCommoditiesCtrl(){
        logger.info("Dealing with CommoditiesCtrl Action...");
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        if (auth != null && auth.getPrincipal() != null) {
            for (GrantedAuthority at : auth.getAuthorities()) {
                authorityMap.put(at.getAuthority(), "true");
            }
        }
        commodities = new ListModelList<Commodity>(iCommodityDao.getList(null, false, 0, Integer.MAX_VALUE, null));
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (authorityMap.get("ROLE_SUPERVISOR") != null) {
            grid.setRowRenderer(new CommodityDataRender());
        }
    }

    @Listen("onClick = #insertCommodities")
    public void insertCommodities() {
        Window win = (Window) Executions.getCurrent().createComponents("/crm/publishCommodity.zul", showCommoditiesWin, null);
        // 设置关闭按钮
        win.setTitle("新增商品");
        win.setBorder("normal");
        win.setClosable(true);
        win.doModal();
    }

    @Listen("onClick = #search")
    public void search() {
        commodities = new ListModelList<Commodity>(iCommodityDao.getList(null, false, 0, Integer.MAX_VALUE, null));
        grid.setModel(commodities);
        grid.setRowRenderer(new CommodityDataRender());
    }

    public ListModel<Commodity> getCommodities() {
        return commodities;
    }
}
