package com.mavenMVC.web.zk.renders;

import com.mavenMVC.dao.*;
import com.mavenMVC.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by yizhou on 2018/1/25.
 */
public class OrdersDataRowRender implements RowRenderer {

    @Autowired
    private IOrderDao iOrderDao;

    @Autowired
    private IUserDao iUserDao;

    @Autowired
    private IAddressDao iAddressDao;

    private Window parentWindow;

    private Map<Long,String> userNameMap = new ConcurrentHashMap<Long, String>();
    private Map<Long,String> addressMap = new ConcurrentHashMap<Long, String>();

    public OrdersDataRowRender() {
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);

        List<User> userNameList = iUserDao.getAllUsers();
        if (userNameList!=null){
            for (User user :userNameList){
                userNameMap.put(user.getUserId(),user.getUserName() == null ? "" : user.getUserName());
            }
        }

        List<Address> addresses = iAddressDao.getList(null,true,0,Integer.MAX_VALUE,null);
        if (addresses != null) {
            for (Address address:addresses) {
                addressMap.put(address.getAddressId(),address.getAddressName());
            }
        }
    }

    @Override
    public void render(Row row, Object o, int i) throws Exception {
        if (o == null)
            return;
        if (o.getClass() == Order.class) {
            final Order orderEntity = (Order) o;
            parentWindow = (Window) row.getParent().getParent().getParent();
            final Long orderId = orderEntity.getOrderId();
            new Label("" + (userNameMap.get(orderEntity.getUserId()))).setParent(row);
            new Label("" + (addressMap.get(orderEntity.getAddressId()))).setParent(row);
            new Label("" + (orderEntity.getOrderMessage() == null?"":orderEntity.getOrderMessage())).setParent(row);
            new Label("" + (orderEntity.getOrderDeliveryId() == null?"":orderEntity.getOrderDeliveryId())).setParent(row);
            String statusString = "";
            switch (orderEntity.getOrderStatus())
            {
                case 0:
                    statusString = "新建";
                    break;
                case 1:
                    statusString = "支付";
                    break;
                case 2:
                    statusString = "退款申请";
                    break;
                case 3:
                    statusString = "已退款";
                    break;
                case 4:
                    statusString = "发货";
                    break;
                case 5:
                    statusString = "结束";
                    break;
            }
            new Label("" + (orderEntity.getOrderStatus() == null?"":statusString)).setParent(row);
            new Label("" + (orderEntity.getOrderPrice() == null?"":orderEntity.getOrderPrice())).setParent(row);
            Div div = new Div();
            final Button thumbBtn = new Button("详情");
            thumbBtn.setParent(div);
            thumbBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    Map<String, String> arg = new HashMap<String, String>();
                    arg.put("orderId", orderEntity.getOrderId().toString());
                    // 新创建一个组件，并且传值 map
                    Window win = (Window) Executions.getCurrent().createComponents("/crm/publishOrder.zul", parentWindow, arg);
                    // 设置关闭按钮
                    win.setTitle("订单详情");
                    win.setBorder("normal");
                    win.setClosable(true);
                    // modal：modal与hightlighted模式基本上是相同的。modal模式下，Window之外的组件是不能够操作的（如下图）。
                    win.doModal();
                    refreshParentWindow();
                }
            });
            div.setParent(row);
        }
    }

    public void refreshParentWindow() {
        Button b = (Button) parentWindow.getFellow("search");
        Events.sendEvent(new Event("onClick", b));
    }
}
