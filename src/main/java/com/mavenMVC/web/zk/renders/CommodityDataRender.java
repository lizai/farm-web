package com.mavenMVC.web.zk.renders;

import com.mavenMVC.dao.*;
import com.mavenMVC.entity.*;
import com.mavenMVC.web.zk.ShowBooksCtrl;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by yizhou on 2018/1/21.
 */

public class CommodityDataRender implements RowRenderer{
    protected final Logger logger = Logger.getLogger(String.valueOf(CommodityDataRender.class));

    @Autowired
    private ICommodityDao iCommodityDao;

    @Autowired
    private ICommodityCategoryDao iCommodityCategoryDao;

    private Window parentWindow;

    private Map<Long,String> commodityCategoryMap = new ConcurrentHashMap<Long, String>();

    public CommodityDataRender() {
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        List<Commoditycategory> commodityCategoryList = iCommodityCategoryDao.getList(null,true,0,Integer.MAX_VALUE,null);
        if(commodityCategoryList!=null){
            for(Commoditycategory commodityCategory : commodityCategoryList){
                commodityCategoryMap.put(commodityCategory.getCommodityCategoryId(),commodityCategory.getCommodityCategoryName());
            }
        }
    }

    @Override
    public void render(Row row, Object o, int i) throws Exception {
        if (o == null)
            return;
        if (o.getClass() == Commodity.class) {
            final Commodity commodityEntity = (Commodity) o;
            parentWindow = (Window) row.getParent().getParent().getParent();
            final Long commodityId = commodityEntity.getCommodityId();
            final Button validBtn = new Button("生效");
            final Button deleteBtn = new Button("删除");

            new Label("" + (commodityEntity.getCommodityName())).setParent(row);
            new Label("" + (commodityEntity.getCommodityPrice())).setParent(row);
            new Label("" + (commodityEntity.getCommodityDetail() == null?"":commodityEntity.getCommodityDetail())).setParent(row);
            if (commodityEntity.getCommodityStatus() == null || commodityEntity.getCommodityStatus() == 0) {
                new Label("" + "销售中" ).setParent(row);
            }else {
                new Label("" + "下架" ).setParent(row);
            }
            if (commodityEntity.getCommodityType() == null || commodityEntity.getCommodityType() == 0) {
                new Label("" + "生活超市商品" ).setParent(row);
            }else if(commodityEntity.getCommodityType() == 1) {
                new Label("" + "装修超市商品" ).setParent(row);
            }else if(commodityEntity.getCommodityType() == 2) {
                new Label("" + "养殖超市商品" ).setParent(row);
            }else {
                new Label("" + "地块" ).setParent(row);
            }
            new Label("" + (commodityCategoryMap.get(commodityEntity.getCommodityCategory()))).setParent(row);
            new Label("" + (commodityEntity.getCommoditySaleNum())).setParent(row);

            Div div = new Div();
            final Button thumbBtn = new Button("详情");
            thumbBtn.setParent(div);
            validBtn.setParent(div);
            deleteBtn.setParent(div);
            thumbBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    Map<String, String> arg = new HashMap<String, String>();
                    arg.put("commodityId", commodityEntity.getCommodityId().toString());
                    // 新创建一个组件，并且传值 map
                    Window win = (Window) Executions.getCurrent().createComponents("/crm/publishCommodity.zul", parentWindow, arg);
                    // 设置关闭按钮
                    win.setTitle("商品详情");
                    win.setBorder("normal");
                    win.setClosable(true);
                    // modal：modal与hightlighted模式基本上是相同的。modal模式下，Window之外的组件是不能够操作的（如下图）。
                    win.doModal();
                    refreshParentWindow();
                }
            });
            validBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    commodityEntity.setCommodityStatus(0);
                    iCommodityDao.saveOrUpdateEntity(commodityEntity);
                    validBtn.setDisabled(true);
                    deleteBtn.setDisabled(false);
                    refreshParentWindow();
                }
            });
            deleteBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    commodityEntity.setCommodityStatus(1);
                    iCommodityDao.saveOrUpdateEntity(commodityEntity);
                    validBtn.setDisabled(false);
                    deleteBtn.setDisabled(true);
                    refreshParentWindow();
                }
            });

            div.setParent(row);
        }
    }

    public void refreshParentWindow() {
        Button b = (Button) parentWindow.getFellow("search");
        Events.sendEvent(new Event("onClick", b));
    }
}

