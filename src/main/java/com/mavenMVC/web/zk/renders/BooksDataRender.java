package com.mavenMVC.web.zk.renders;

import com.mavenMVC.dao.*;
import com.mavenMVC.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BooksDataRender implements RowRenderer {

    @Autowired
    private IBookDao iBookDao;

    @Autowired
    private ITableTypeDao iTableTypeDao;

    @Autowired
    private IUserDao iUserDao;

    private Window parentWindow;

    private Map<Long,String> tableTypeMap = new ConcurrentHashMap<Long, String>();

    private Map<Long,String> userNameMap = new ConcurrentHashMap<Long, String>();

    public BooksDataRender() {
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        List<Tabletype> tableTypeList = iTableTypeDao.getList(null,true,0,Integer.MAX_VALUE,null);
        if(tableTypeList!=null){
            for(Tabletype tableType : tableTypeList){
                tableTypeMap.put(tableType.getTableTypeId(),tableType.getTableTypeName());
            }
        }

        List<User> userNameList = iUserDao.getAllUsers();
        if (userNameList!=null){
            for (User user :userNameList){
                userNameMap.put(user.getUserId(),user.getUserName() == null ? "" : user.getUserName());
            }
        }
    }

    @Override
    public void render(Row row, Object o, int i) throws Exception {
        if (o == null)
            return;
        if (o.getClass() == Book.class) {
            final Book bookEntity = (Book) o;
            parentWindow = (Window) row.getParent().getParent().getParent();
            final Long bookId = bookEntity.getBookId();
            final Button validBtn = new Button("生效");
            final Button deleteBtn = new Button("删除");
            new Label("" + (userNameMap.get(bookEntity.getUserId()))).setParent(row);
            new Label("" + (bookEntity.getBookNum() == null ? "" : bookEntity.getBookNum())).setParent(row);
            new Label("" + (tableTypeMap.get(bookEntity.getBookTableType()))).setParent(row);
            new Label("" + (bookEntity.getBookName())).setParent(row);
            new Label("" + (bookEntity.getBookCellphone())).setParent(row);
            new Label("" + (bookEntity.getBookMsg())).setParent(row);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
            if (bookEntity.getDinnerTime() == null) {
                new Label("" ).setParent(row);
            }else {
                new Label("" + sdf.format(new Date(bookEntity.getDinnerTime()))).setParent(row);
            }
            new Label("" + sdf.format(new Date(bookEntity.getCreateTime()))).setParent(row);
            if (bookEntity.getBookStatus() == null || bookEntity.getBookStatus() == 0) {
                new Label("" + "生效" ).setParent(row);
            }else {
                new Label("" + "取消").setParent(row);
            }
            Div div = new Div();
            final Button thumbBtn = new Button("详情");
            thumbBtn.setParent(div);
            validBtn.setParent(div);
            deleteBtn.setParent(div);
            thumbBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    Map<String, String> arg = new HashMap<String, String>();
                    arg.put("bookId", bookEntity.getBookId().toString());
                    arg.put("userName",userNameMap.get(bookEntity.getUserId()));
                    // 新创建一个组件，并且传值 map
                    Window win = (Window) Executions.getCurrent().createComponents("/crm/publishBook.zul", parentWindow, arg);
                    // 设置关闭按钮
                    win.setTitle("预约详情");
                    win.setBorder("normal");
                    win.setClosable(true);
                    // modal：modal与hightlighted模式基本上是相同的。modal模式下，Window之外的组件是不能够操作的（如下图）。
                    win.doModal();
                    refreshParentWindow();
                }
            });
            validBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    bookEntity.setBookStatus(0);
                    iBookDao.saveOrUpdateEntity(bookEntity);
                    validBtn.setDisabled(true);
                    deleteBtn.setDisabled(false);
                    refreshParentWindow();
                }
            });
            deleteBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    bookEntity.setBookStatus(1);
                    iBookDao.saveOrUpdateEntity(bookEntity);
                    validBtn.setDisabled(false);
                    deleteBtn.setDisabled(true);
                    refreshParentWindow();
                }
            });
            div.setParent(row);
        }
    }

    public void refreshParentWindow() {
        Button b = (Button) parentWindow.getFellow("search");
        Events.sendEvent(new Event("onClick", b));
    }
}
