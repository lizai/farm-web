package com.mavenMVC.web.zk.renders;

import com.mavenMVC.dao.IQueueDao;
import com.mavenMVC.dao.ITableTypeDao;
import com.mavenMVC.dao.IUserDao;
import com.mavenMVC.entity.Book;
import com.mavenMVC.entity.Queue;
import com.mavenMVC.entity.Tabletype;
import com.mavenMVC.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by yizhou on 2018/1/23.
 */
public class QueueDataRender  implements RowRenderer {

    @Autowired
    private IQueueDao iQueueDao;

    @Autowired
    private ITableTypeDao iTableTypeDao;

    @Autowired
    private IUserDao iUserDao;

    private Window parentWindow;
    private Map<Long,String> userNameMap = new ConcurrentHashMap<Long, String>();
    private Map<Long,String> tableTypeMap = new ConcurrentHashMap<Long, String>();

    public QueueDataRender() {
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        List<Tabletype> tableTypeList = iTableTypeDao.getList(null,true,0,Integer.MAX_VALUE,null);
        if(tableTypeList!=null){
            for(Tabletype tableType : tableTypeList){
                tableTypeMap.put(tableType.getTableTypeId(),tableType.getTableTypeName());
            }
        }

        List<User> userNameList = iUserDao.getAllUsers();
        if (userNameList!=null){
            for (User user :userNameList){
                userNameMap.put(user.getUserId(),user.getUserName() == null ? "" : user.getUserName());
            }
        }
    }


    @Override
    public void render(Row row, Object o, int i) throws Exception {
        if (o == null)
            return;
        if (o.getClass() == Queue.class) {
            final Queue queueEntity = (Queue) o;
            parentWindow = (Window) row.getParent().getParent().getParent();

            new Label("" + (queueEntity.getQueueId())).setParent(row);
            if (queueEntity.getUserId() == null) {
                new Label("").setParent(row);
            }else {
                new Label("" + (userNameMap.get(queueEntity.getUserId()))).setParent(row);
            }
            if (queueEntity.getQueueTabelType() == null ||queueEntity.getQueueTabelType() == 0) {
                new Label("" + (tableTypeMap.get(new Long(1)))).setParent(row);
            }else {
                new Label("" + (tableTypeMap.get(queueEntity.getQueueTabelType()))).setParent(row);
            }
            if (queueEntity.getQueueCalled() == null ||queueEntity.getQueueCalled() == 0) {
                new Label( "待叫号").setParent(row);
            }else {
                new Label( "已叫号").setParent(row);
            }

            Div div = new Div();
            final Button thumbBtn = new Button("叫号");
            thumbBtn.setParent(div);
            thumbBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    queueEntity.setQueueCalled(1);
                    iQueueDao.saveOrUpdateEntity(queueEntity);
                    thumbBtn.setDisabled(true);
                    refreshParentWindow();
                }
            });
            div.setParent(row);
        }
    }
    public void refreshParentWindow() {
        Button b = (Button) parentWindow.getFellow("search");
        Events.sendEvent(new Event("onClick", b));
    }
}

