package com.mavenMVC.web.zk;

import com.mavenMVC.dao.IBookDao;
import com.mavenMVC.dao.IQueueDao;
import com.mavenMVC.dao.ITableTypeDao;
import com.mavenMVC.entity.Book;
import com.mavenMVC.entity.Queue;
import com.mavenMVC.entity.Tabletype;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Created by yizhou on 2018/1/23.
 */
public class PublishQueueViewCtrl  extends SelectorComposer<Component> {

    protected final Logger logger = Logger.getLogger(String.valueOf(PublishQueueViewCtrl.class));

    @Wire
    private Window win;

    @Wire
    private Selectbox typeSelectbox;

    @Autowired
    private IQueueDao iQueueDao;

    @Autowired
    private ITableTypeDao iTableTypeDao;

    private Window parentWindow;

    private ListModel<Tabletype> tableTypes;

    private Queue queueEntity;


    public PublishQueueViewCtrl() {
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        Map map = Executions.getCurrent().getArg();
        tableTypes = new ListModelList<Tabletype>(iTableTypeDao.getList(null, true, 0, Integer.MAX_VALUE, null));
        queueEntity = new Queue();
        queueEntity.setQueueTabelType(new Long(1));
        ((ListModelList<Tabletype>) tableTypes).addToSelection(((ListModelList<Tabletype>) tableTypes).get(0));
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        logger.info("Dealing with PublishBookViewCtrl Action...");
        parentWindow = (Window) win.getParent();
    }

    @Listen("onSelect = #typeSelectbox")
    public void typeSelectbox() {
        Set<Tabletype> types = ((ListModelList<Tabletype>) tableTypes).getSelection();
        Tabletype type = types.iterator().next();
        queueEntity.setQueueTabelType(type.getTableTypeId());
    }

    @Listen("onClick = #submitBtn")
    public void submit() {
        logger.info("do submit function");
            iQueueDao.saveOrUpdateEntity(queueEntity);
            Messagebox.show("预约信息修改成功");
            win.detach();
            refreshParentWindow();
    }

    public void refreshParentWindow() {
        Button b = (Button) parentWindow.getFellow("search");
        Events.sendEvent(new Event("onClick", b));
    }

    public ListModel<Tabletype> getTableTypes() {
        return tableTypes;
    }

}
