package com.mavenMVC.web.zk;

import com.mavenMVC.dao.IDishOrderDao;
import com.mavenMVC.dao.IOrderDao;
import com.mavenMVC.entity.Book;
import com.mavenMVC.entity.Dishorder;
import com.mavenMVC.entity.Order;
import com.mavenMVC.web.zk.renders.DishOrderDataRowRender;
import com.mavenMVC.web.zk.renders.OrdersDataRowRender;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.util.HashMap;
import java.util.Map;


public class ShowOrdersCtrl extends SelectorComposer<Component> {

    protected final Logger logger = Logger.getLogger(String.valueOf(ShowOrdersCtrl.class));

    @Wire
    private Window showOrdersWin;
    @Wire
    private Grid grid;
    @Wire
    private Button search;

    @Autowired
    private IOrderDao iOrderDao;

    private ListModel<Order> orders;

    private Map authorityMap = new HashMap();


    public ShowOrdersCtrl() {
        logger.info("Dealing with ShowOrdersCtrl Action...");
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        if (auth != null && auth.getPrincipal() != null) {
            for (GrantedAuthority at : auth.getAuthorities()) {
                authorityMap.put(at.getAuthority(), "true");
            }
        }
        orders = new ListModelList<Order>(iOrderDao.getList(null, true, 0, Integer.MAX_VALUE, null));
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (authorityMap.get("ROLE_SUPERVISOR") != null) {
            grid.setRowRenderer(new OrdersDataRowRender());
        }
    }

    @Listen("onClick = #search")
    public void search() {
        orders = new ListModelList<Order>(iOrderDao.getList(null, true, 0, Integer.MAX_VALUE, null));
        grid.setModel(orders);
        grid.setRowRenderer(new OrdersDataRowRender());
    }

    public ListModel<Order> getOrders() {
        return orders;
    }

}
