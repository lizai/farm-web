package com.mavenMVC.web.zk;

import com.mavenMVC.dao.IBookDao;
import com.mavenMVC.dao.IQueueDao;
import com.mavenMVC.entity.Book;
import com.mavenMVC.entity.Queue;
import com.mavenMVC.web.zk.renders.BooksDataRender;
import com.mavenMVC.web.zk.renders.QueueDataRender;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yizhou on 2018/1/23.
 */
public class ShowQueueCtrl  extends SelectorComposer<Component> {
    private static final long serialVersionUID = 1L;

    protected final Logger logger = Logger.getLogger(String.valueOf(ShowQueueCtrl.class));

    @Wire
    private Window showQueueWin;
    @Wire
    private Button addQueue;
    @Wire
    private Button search;
    @Wire
    private Grid grid;

    @Autowired
    private IQueueDao iQueueDao;

    private ListModel<Queue> queues;

    private Map authorityMap = new HashMap();

    public ShowQueueCtrl() {
        logger.info("Dealing with QueueCtrl Action...");
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getPrincipal() != null) {
            for (GrantedAuthority at : auth.getAuthorities()) {
                authorityMap.put(at.getAuthority(), "true");
            }
        }
        queues = new ListModelList<Queue>(iQueueDao.getList(null, true, 0, Integer.MAX_VALUE, null));
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (authorityMap.get("ROLE_SUPERVISOR") != null) {
            grid.setRowRenderer(new QueueDataRender());
        }
    }

    @Listen("onClick = #addQueue")
    public void insertDishOrder() {
        Window win = (Window) Executions.getCurrent().createComponents("/crm/publishQueue.zul", showQueueWin, null);
        // 设置关闭按钮
        win.setTitle("增加排号");
        win.setBorder("normal");
        win.setClosable(true);
        win.doModal();
    }

    @Listen("onClick = #search")
    public void search() {
        queues = new ListModelList<Queue>(iQueueDao.getList(null, true, 0, Integer.MAX_VALUE, null));
        grid.setModel(queues);
        grid.setRowRenderer(new QueueDataRender());
    }

    public ListModel<Queue> getQueues() {
        return queues;
    }

}
