package com.mavenMVC.web.zk;

import com.mavenMVC.dao.IOrderDao;
import com.mavenMVC.entity.Order;
import com.mavenMVC.entity.OrderStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by yizhou on 2018/1/25.
 */
public class PublishOrderViewCtrl   extends SelectorComposer<Component> {

    protected final Logger logger = Logger.getLogger(String.valueOf(PublishOrderViewCtrl.class));

    @Wire
    private Window win;

    @Wire
    private Selectbox statusSelectbox;

    @Autowired
    private IOrderDao iOrderDao;

    private Window parentWindow;

    private ListModel<OrderStatus> statusTypes;

    private Order orderEntity;


    public PublishOrderViewCtrl() {
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        Map map = Executions.getCurrent().getArg();
        String orderId = map.get("orderId").toString();
        orderEntity = iOrderDao.getOrderById(new Long(orderId));
        List<OrderStatus> orderStatusList = new ArrayList<OrderStatus>();
        orderStatusList.add(new OrderStatus(0,"新建"));
        orderStatusList.add(new OrderStatus(1,"支付"));
        orderStatusList.add(new OrderStatus(2,"退款申请"));
        orderStatusList.add(new OrderStatus(3,"已退款"));
        orderStatusList.add(new OrderStatus(4,"发货"));
        orderStatusList.add(new OrderStatus(5,"结束"));

        statusTypes = new ListModelList<OrderStatus>(orderStatusList);

        for(OrderStatus i : ((ListModelList<OrderStatus>) statusTypes)){
            if(i.getStatusId().equals(orderEntity.getOrderStatus())){
                ((ListModelList<OrderStatus>)statusTypes).addToSelection(i);
            }
        }
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        logger.info("Dealing with PublishBookViewCtrl Action...");
        parentWindow = (Window) win.getParent();
    }

    @Listen("onSelect = #typeSelectbox")
    public void typeSelectbox() {
        Set<OrderStatus> types = ((ListModelList<OrderStatus>) statusTypes).getSelection();
        OrderStatus type = types.iterator().next();
        orderEntity.setOrderStatus(type.getStatusId().intValue());
    }

    @Listen("onClick = #submitBtn")
    public void submit() {
        logger.info("do submit function");
        iOrderDao.saveOrUpdateOrder(orderEntity);
        Messagebox.show("预约信息修改成功");
        win.detach();
        refreshParentWindow();
    }

    public void refreshParentWindow() {
        Button b = (Button) parentWindow.getFellow("search");
        Events.sendEvent(new Event("onClick", b));
    }

    public ListModel<OrderStatus> getStatusTypes() {
        return statusTypes;
    }
}
