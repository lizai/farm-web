package com.mavenMVC.web.zk;

import com.mavenMVC.dao.IBookDao;
import com.mavenMVC.dao.IDishDao;
import com.mavenMVC.dao.IDishTypeDao;
import com.mavenMVC.dao.ITableTypeDao;
import com.mavenMVC.entity.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.io.Files;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.*;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by yizhou on 2018/1/20.
 */
public class PublishBookViewCtrl extends SelectorComposer<Component> {
    protected final Logger logger = Logger.getLogger(String.valueOf(PublishBookViewCtrl.class));
    @Wire
    private Window win;
    @Wire
    private Intbox bookNumIntbox;
    @Wire
    private Selectbox typeSelectbox;
    @Wire
    private Textbox bookNameTextbox;
    @Wire
    private Textbox bookCellphoneTextbox;
    @Wire
    private Textbox bookMsgTextbox;
    @Wire
    private Textbox dinnerTimeTextbox;
    @Wire
    private Button submitBtn;

    @Autowired
    private IBookDao iBookDao;
    @Autowired
    private ITableTypeDao iTableTypeDao;

    private Window parentWindow;

    private ListModel<Tabletype> tableTypes;

    private Book bookEntity;

    private String userName;

    private String dinnerTime;

    public PublishBookViewCtrl() {
        SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(this);
        Map map = Executions.getCurrent().getArg();
        tableTypes = new ListModelList<Tabletype>(iTableTypeDao.getList(null, true, 0, Integer.MAX_VALUE, null));
        userName = map.get("userName") == null?"":map.get("userName").toString();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
        if(map.get("bookId")!=null){
            bookEntity = iBookDao.getById(Long.parseLong(map.get("bookId").toString()));
            for(Tabletype i : ((ListModelList<Tabletype>) tableTypes)){
                if(i.getTableTypeId().equals(bookEntity.getBookTableType())){
                    ((ListModelList<Tabletype>)tableTypes).addToSelection(i);
                }
            }
            if (bookEntity.getDinnerTime() != null) {
                dinnerTime = "" + sdf.format(new Date(bookEntity.getDinnerTime()));
            }else {
                dinnerTime = sdf.format(new Date());
            }
        }else{
            bookEntity = new Book();
            bookEntity.setBookMsg("");
            bookEntity.setBookName("");
            bookEntity.setBookNum(1);
            bookEntity.setUserId(new Long(0));
            bookEntity.setBookTableType(tableTypes.getElementAt(0).getTableTypeId());
            ((ListModelList<Tabletype>) tableTypes).addToSelection(((ListModelList<Tabletype>) tableTypes).get(0));
            dinnerTime = sdf.format(new Date());
        }
    }


    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        logger.info("Dealing with PublishBookViewCtrl Action...");
        parentWindow = (Window) win.getParent();
    }

    @Listen("onChange = #bookNumIntbox")
    public void bookNumIntbox() {
        Integer bookNum = bookNumIntbox.getValue();
        bookEntity.setBookNum(bookNum);
    }

    @Listen("onChange = #bookNameTextbox")
    public void bookNameTextbox() {
        String name = bookNameTextbox.getValue();
        bookEntity.setBookName(name);
    }

    @Listen("onChange = #bookCellphoneTextbox")
    public void bookCellphoneTextbox() {
        String cellphone = bookCellphoneTextbox.getValue();
        bookEntity.setBookCellphone(cellphone);
    }

    @Listen("onChange = #bookMsgTextbox")
    public void bookMsgTextbox() {
        String msg = bookMsgTextbox.getValue();
        bookEntity.setBookMsg(msg);
    }

    @Listen("onChange = #dinnerTimeTextbox")
    public void dinnerTimeTextbox() {
//        try{
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//            Date date = simpleDateFormat.parse(dinnerTimeTextbox.getValue().trim());
//            long ts = date.getTime();
//            bookEntity.setDinnerTime(ts);
//        }catch (ParseException e){
//            logger.error(e.getStackTrace(),e);
//            Messagebox.show("时间格式有误，请填写为:yyyy-MM-dd HH:mm");
//        }
    }

    @Listen("onSelect = #typeSelectbox")
    public void typeSelectbox() {
        Set<Tabletype> types = ((ListModelList<Tabletype>) tableTypes).getSelection();
        Tabletype type = types.iterator().next();
        bookEntity.setBookTableType(type.getTableTypeId());
    }

    @Listen("onClick = #submitBtn")
    public void submit() {
        logger.info("do submit function");
        try{
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date = simpleDateFormat.parse(dinnerTimeTextbox.getValue().trim());
            long ts = date.getTime();
            bookEntity.setDinnerTime(ts);
            iBookDao.saveOrUpdateEntity(bookEntity);
            Messagebox.show("预约信息修改成功");
            win.detach();
            refreshParentWindow();
        }catch (ParseException e){
            logger.error(e.getStackTrace(),e);
            Messagebox.show("时间格式有误，请填写为:yyyy-MM-dd HH:mm");
        }
    }
    public void refreshParentWindow() {
        Button b = (Button) parentWindow.getFellow("search");
        Events.sendEvent(new Event("onClick", b));
    }

    public Book getBookEntity() {
        return bookEntity;
    }

    public String getUserName() { return userName; }

    public ListModel<Tabletype> getTableTypes() {
        return tableTypes;
    }

    public String getDinnerTime() { return dinnerTime; }
//    public static void main(String[] args){
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        Date date = null;
//        try {
//            date = simpleDateFormat.parse("2018-01-20 19:00");
//            long ts = date.getTime();
//            System.out.println(ts);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//    }
}


