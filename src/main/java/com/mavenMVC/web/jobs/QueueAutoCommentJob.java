package com.mavenMVC.web.jobs;

import com.mavenMVC.dao.IQueueDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by lizai on 15/7/9.
 */

@Component
public class QueueAutoCommentJob {

    protected final Logger logger = Logger.getLogger(String.valueOf(QueueAutoCommentJob.class));

    public QueueAutoCommentJob(){
        logger.info("QueueAutoCommentJob init.......");
    }

    @Autowired
    private IQueueDao iQueueDao;

    @Scheduled(cron="0 0 5 * * ? ")
    public void run() {
        logger.info("QueueAutoCommentJob running");
        iQueueDao.deleteAll();
        logger.info("QueueAutoCommentJob finished");
    }
}
