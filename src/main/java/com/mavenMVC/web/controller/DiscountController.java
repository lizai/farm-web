package com.mavenMVC.web.controller;

import com.mavenMVC.authorization.annotation.CurrentUser;
import com.mavenMVC.dao.IDiscountDao;
import com.mavenMVC.entity.Discount;
import com.mavenMVC.entity.User;
import com.mavenMVC.util.RequestManager;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by lizai on 15/6/25.
 */
@Controller
@RequestMapping("/discount")
public class DiscountController extends BaseController {

    protected final Logger logger = Logger.getLogger(String.valueOf(DiscountController.class));

    @Autowired
    private IDiscountDao iDiscountDao;

    @RequestMapping(value = "/getDiscount", produces = "text/json; charset=utf-8", method = {RequestMethod.POST, RequestMethod.GET})
//    @Authorization
    @ApiOperation(value = "获取折扣")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "sig", value = "sig", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "requestTime", value = "requestTime", required = true, dataType = "string", paramType = "header")
    })
    public
    @ResponseBody
    String getDiscount(@CurrentUser Object currentUser) {
        RequestManager requestManager = new RequestManager();
        JSONObject result = new JSONObject();
        try {
            logger.info("Dealing with getDiscount Action...");
            Long userId;
            Assert.notNull(currentUser, "未登录系统");
            if (currentUser instanceof User) {
                userId = ((User) currentUser).getUserId();
            } else {
                throw new Exception("当前用户类型错误");
            }
            Discount discount = iDiscountDao.getById((long)1);
            result = JSONObject.fromObject(discount);
        } catch (Exception e) {
            logger.error(e.getStackTrace(),e);
            requestManager.putErrorMessage(e.getMessage());
        } finally {
            logger.info("Done getDiscount Action!");
            return requestManager.printJson(result).toString();
        }
    }

}
