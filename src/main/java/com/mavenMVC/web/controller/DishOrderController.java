package com.mavenMVC.web.controller;

import com.mavenMVC.authorization.annotation.CurrentUser;
import com.mavenMVC.dao.IDiscountDao;
import com.mavenMVC.dao.IDishDao;
import com.mavenMVC.dao.IDishOrderDao;
import com.mavenMVC.dao.ITableTypeDao;
import com.mavenMVC.entity.*;
import com.mavenMVC.service.IDishService;
import com.mavenMVC.util.RequestManager;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lizai on 15/6/25.
 */
@Controller
@RequestMapping("/dishOrder")
public class DishOrderController extends BaseController {

    protected final Logger logger = Logger.getLogger(String.valueOf(DishOrderController.class));

    @Autowired
    private IDishService iDishService;

    @Autowired
    private IDishOrderDao iDishOrderDao;

    @Autowired
    private IDishDao iDishDao;

    @Autowired
    private ITableTypeDao iTableTypeDao;

    @Autowired
    private IDiscountDao iDiscountDao;

    @RequestMapping(value = "/createDishOrder", produces = "text/json; charset=utf-8", method = {RequestMethod.POST, RequestMethod.GET})
//    @Authorization
    @ApiOperation(value = "点餐下单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "sig", value = "sig", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "requestTime", value = "requestTime", required = true, dataType = "string", paramType = "header")
    })
    public
    @ResponseBody
    String createDishOrder(@CurrentUser Object currentUser,
                           @ApiParam(name = "dishOrderId", value = "订单id，可不传，不传是为新建")
                           @RequestParam(value = "dishOrderId", required = false) Long dishOrderId,
                           @ApiParam(name = "dishIds", value = "下单菜品id，以,分隔")
                           @RequestParam(value = "dishIds") String dishIds,
                           @ApiParam(name = "price", value = "总金额")
                           @RequestParam(value = "price") Integer price) {
        RequestManager requestManager = new RequestManager();
        JSONObject result = new JSONObject();
        try {
            logger.info("Dealing with createDinnerOrder Action...");
            Long userId;
            Assert.notNull(currentUser, "未登录系统");
            if (currentUser instanceof User) {
                userId = ((User) currentUser).getUserId();
            } else {
                throw new Exception("当前用户类型错误");
            }
            Dishorder dishorder = null;
            if (dishOrderId != null && dishOrderId > 0) {
                dishorder = iDishOrderDao.getById(dishOrderId);
                if (dishorder.getDishOrderStatus() != 0) {
                    throw new Exception("订单状态不可修改");
                }
                String oldIds = dishorder.getDishIds();
                String newIds = "";
                if(oldIds != null && !oldIds.trim().equals("")){
                    newIds = oldIds + "," + dishIds;
                }else{
                    newIds = dishIds;
                }
                dishorder.setDishIds(newIds);
                Integer oldPrice = dishorder.getDishOrderPrice();
                Integer newPrice = 0;
                if(oldPrice != null && oldPrice > 0){
                    newPrice = oldPrice + price;
                }else{
                    newPrice = price;
                }
                dishorder.setDishOrderPrice(newPrice);
            } else {
                List<String> cols = new ArrayList<String>();
                List<Object> contents = new ArrayList<Object>();
                cols.add(Dishorder.PROPERTYNAME_USER_ID);
                cols.add(Dishorder.PROPERTYNAME_DISH_ORDER_STATUS);
                contents.add(userId);
                contents.add(0);
                List<Dishorder> dishorders = iDishOrderDao.getListByAndColumns(contents, cols, 0, Integer.MAX_VALUE, null, Dishorder.PROPERTYNAME_CREATE_TIME, true);
                if (dishorders != null && dishorders.size() > 0) {
                    throw new Exception("上一单尚未支付");
                }
                dishorder = new Dishorder();
                dishorder.setDishIds(dishIds);
                dishorder.setDishOrderPrice(price);
                dishorder.setDishOrderStatus(0);
                dishorder.setUserId(userId);
            }
            iDishOrderDao.saveOrUpdateEntity(dishorder);
            result = JSONObject.fromObject(dishorder);
        } catch (Exception e) {
            requestManager.putErrorMessage(e.getMessage());
        } finally {
            logger.info("Done createDishOrder Action!");
            return requestManager.printJson(result).toString();
        }
    }

    @RequestMapping(value = "/getMyDishOrder", produces = "text/json; charset=utf-8", method = {RequestMethod.POST, RequestMethod.GET})
//    @Authorization
    @ApiOperation(value = "获取我的点餐")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "sig", value = "sig", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "requestTime", value = "requestTime", required = true, dataType = "string", paramType = "header")
    })
    public
    @ResponseBody
    String getMyDishOrder(@CurrentUser Object currentUser,
                          @RequestParam(value = "start") Integer start,
                          @RequestParam(value = "offset") Integer offset,
                          @RequestParam(value = "receivedIds", required = false) List<Long> receivedIds,
                          @ApiParam(name = "current", value = "获取当前进行的餐单为1，获取全部餐单为0")
                          @RequestParam(value = "current") Integer current) {
        RequestManager requestManager = new RequestManager();
        JSONArray result = new JSONArray();
        try {
            logger.info("Dealing with getMyDishOrder Action...");
            Long userId;
            Assert.notNull(currentUser, "未登录系统");
            if (currentUser instanceof User) {
                userId = ((User) currentUser).getUserId();
            } else {
                throw new Exception("当前用户类型错误");
            }
            if(current == null || current > 1 || current < 0)
                throw new Exception("current值有误");
            List<Dishorder> dishOrderes = new ArrayList<Dishorder>();
            if(current == 0){
                dishOrderes = iDishOrderDao.getListByColumn(userId, Dishorder.PROPERTYNAME_USER_ID, 0, Integer.MAX_VALUE, null, Dishorder.PROPERTYNAME_CREATE_TIME, true);
            }else{
                List<String> cols = new ArrayList<String>();
                List<Object> contents = new ArrayList<Object>();
                cols.add(Dishorder.PROPERTYNAME_USER_ID);
                cols.add(Dishorder.PROPERTYNAME_DISH_ORDER_STATUS);
                contents.add(userId);
                contents.add(0);
                List<Dishorder> temp = iDishOrderDao.getListByAndColumns(contents, cols, 0, Integer.MAX_VALUE, null, Dishorder.PROPERTYNAME_CREATE_TIME, true);
                if(temp != null && temp.size() > 0)
                    dishOrderes.add(temp.get(0));
            }
            List<Tabletype> tabletypes = iTableTypeDao.getList(Tabletype.PROPERTYNAME_TABLE_TYPE_ID, true, 0, Integer.MAX_VALUE, null);
            if(dishOrderes != null){
                Discount discount = iDiscountDao.getById((long)1);
                for (Dishorder dishorder : dishOrderes) {
                    JSONObject json = JSONObject.fromObject(dishorder);
                    if (dishorder.getDishIds() != null) {
                        List<Long> dishIds = new ArrayList<Long>();
                        String idString = dishorder.getDishIds().trim();
                        String[] idArray = idString.split(",");
                        HashMap<Long, Integer> count = new HashMap<Long, Integer>();
                        for (String idS : idArray) {
                            Long id = Long.parseLong(idS);
                            dishIds.add(id);
                            Integer c = count.get(id);
                            if (c == null) {
                                count.put(id, 1);
                            } else {
                                count.put(id, c + 1);
                            }
                        }
                        List<Dish> dishes = iDishDao.getListInIds(dishIds);
                        JSONArray jsonArray = new JSONArray();
                        for (Dish dish : dishes) {
                            JSONObject j = JSONObject.fromObject(dish);
                            j.put("count", count.get(dish.getDishId()));
                            jsonArray.add(j);
                        }
                        json.put("dishEntities", jsonArray);
                    }
                    if(discount != null){
                        json.put("discount",discount.getDiscount());
                    }
                    result.add(json);
                }
            }
        } catch (Exception e) {
            logger.error(e.getStackTrace(),e);
            requestManager.putErrorMessage(e.getMessage());
        } finally {
            logger.info("Done getMyDishOrder Action!");
            return requestManager.printJson(result).toString();
        }
    }

}
