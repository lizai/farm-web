package com.mavenMVC.entity;

/**
 * Created by yizhou on 2018/1/25.
 */
public class OrderStatus implements java.io.Serializable {

    private Integer statusId;

    private String statusDetail;

    public OrderStatus(Integer statusId, String statusDetail) {
        this.statusId = statusId;
        this.statusDetail = statusDetail;
    }

    public Integer getStatusId() {
        return this.statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusDetail() {
        return this.statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }


}
