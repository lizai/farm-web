package com.mavenMVC.entity;

/**
 * Created by lizai on 2018/1/25.
 */
public class CommodityType {

    private Integer id;

    private String name;

    public CommodityType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
